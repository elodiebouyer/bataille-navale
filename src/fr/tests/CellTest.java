package fr.tests;

import fr.batailleNavale.game.grid.Cell;

/**
 * Created by elodie on 08/11/13.
 */
public class CellTest {

    private static Cell cell;

    public static void init()
    {
        cell = new Cell(7,7);
    }

    public static void coordinateTest()
    {
        System.out.println("\n Coordinate tests :");
        System.out.println("Get coordinates      : "+((cell.getX()==7 && cell.getY()==7)));
        cell.setX(0);
        cell.setY(2);
        System.out.println("Set coordinates      : "+(cell.getX()==0 && cell.getY()==2));
    }

    public static void inGridTest()
    {
        System.out.println("\n In grid tests :");
        Cell good = new Cell(8,8);
        Cell bad = new Cell(-1,9);
        Cell bad2 = new Cell(9,9);
        System.out.println("(8,8) in grid        : "+ (good.inGrid()==true));
        System.out.println("(-1,9) not in grid   : "+ (bad.inGrid()==false));
        System.out.println("(9,9) not in grid    : "+ (bad.inGrid()==false));
    }

    public static void equalTest()
    {
        System.out.println("\n Equal tests :");
        Cell first = new Cell(4,8);
        Cell second = new Cell(3,2);
        Cell third = new Cell(4,8);
        Cell fourth = new Cell(8,4);
        System.out.println("(4,8) != (3,2)       : "+(first.equals(second) == false));
        System.out.println("(4,8) == 4,8)        : "+(first.equals(third)== true));
        System.out.println("(4,8) != (8,4)       : "+(third.equals(fourth)==false));
    }

    public static void haschCodeTest()
    {
        System.out.println("\n HashCode tests :");
        Cell first = new Cell(4,8);
        Cell second = new Cell(8,4);
        Cell third = new Cell(4,8);
        System.out.println("(4,8) != (8,4)       : "+(first.hashCode() != second.hashCode()));
        System.out.println("(4,8) == (4,8)       : "+(first.hashCode() == third.hashCode()));
    }
}
