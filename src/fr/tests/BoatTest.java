package fr.tests;

import java.util.HashMap;

import fr.batailleNavale.game.boat.*;
import fr.batailleNavale.game.grid.Cell;

/**
 * Created by elodie on 08/11/13.
 */
public class BoatTest {

    private static IBoat battleship;
    private static IBoat carrier;
    private static IBoat cruiser;
    private static IBoat destroyer;
    private static IBoat submarine;

    public static void init()
    {
        battleship = new Battleship();
        carrier = new AircraftCarrier();
        cruiser = new Cruiser();
        destroyer = new Destroyer();
        submarine = new Submarine();
    }

    public static void sizeTest()
    {
        System.out.println("\n Size tests :");
        System.out.println("Battleship size      : "+ (battleship.getSize()==4));
        System.out.println("AircraftCarrier size : "+(carrier.getSize() ==5));
        System.out.println("Cruiser size         : "+(cruiser.getSize() ==3));
        System.out.println("Destroyer size       : "+ (destroyer.getSize()==2));
        System.out.println("Submarine size       : "+(submarine.getSize() ==3));
    }

    public static void noSankTest()
    {
        System.out.println("\n No sank tests :");
        System.out.println("Battleship sank      : "+ (battleship.isSank()==false));
        System.out.println("AircraftCarrier sank : "+ (carrier.isSank()==false));
        System.out.println("Cruiser sank         : "+ (cruiser.isSank()==false));
        System.out.println("Destroyer sank       : "+ (destroyer.isSank()==false));
        System.out.println("Submarine sank       : "+ (submarine.isSank()==false));
    }

    public static void placeBoatTest()
    {
        System.out.println("\n Place boat tests :");
        destroyer.placeBoat(new Cell(4, 4), 0);
        System.out.println("Boat placed          : "+
                (destroyer.isInCell(new Cell(4, 4))==true &&
                 destroyer.isInCell(new Cell(5, 4))==true));
    }

    public static void attackBoat()
    {
        System.out.println("\n Attack boat tests :");
        destroyer.partIsTouch(new Cell(4,4));
        destroyer.partIsTouch(new Cell(5,4));
        HashMap<Cell,Boolean> emplacement = destroyer.getPosition();
        System.out.println("Nb position = 2      : "+ (emplacement.size() == 2));
        System.out.println("Boat attacked        : "+ (emplacement.get(new Cell(4, 4)) == true));
        System.out.println("Boat attacked        : "+ (emplacement.get(new Cell(5,4)) == true));
    }

    public static void sankTest()
    {
        System.out.println("\n Sank boat tests :");
        System.out.println("Boat sank            : "+(destroyer.isSank() == true));
    }
}
