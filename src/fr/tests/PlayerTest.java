package fr.tests;

import java.util.Iterator;
import java.util.Set;

import fr.batailleNavale.game.boat.IBoat;
import fr.batailleNavale.game.grid.Cell;
import fr.batailleNavale.game.players.IPlayer;
import fr.batailleNavale.game.players.PlayerHuman;
import fr.batailleNavale.game.players.PlayerIA;

/**
 * Created by elodie on 08/11/13.
 */
public class PlayerTest {

    private static IPlayer human;
    private static IPlayer ia;

    public static void init()
    {
        human = new PlayerHuman("Human");
        ia = new PlayerIA("Ia");
    }

    public static void nbBoatTest()
    {
        System.out.println("\n Nb boat tests :");
        System.out.println("Human boat           : "+(human.getListBoat().size()==5));
        System.out.println("Ia boat              : "+(ia.getListBoat().size()==5));
    }

    public static void placeBoatHumanTest()
    {
        System.out.println("\n Place boat human tests :");
        System.out.println("Place (4,0)          : " + (human.placeBoat(human.getListBoat().get(0),
                new Cell(4, 0), 0) == true));
        System.out.println("Place ok (7,8)       : " + (human.placeBoat(human.getListBoat().get(2),
                new Cell(7,8),0) == true));
        System.out.println("Place ok (0,6)       : " + (human.placeBoat(human.getListBoat().get(2),
                new Cell(0,6),1) == true));
        System.out.println("Place not ok (-1,6)  : " + (human.placeBoat(human.getListBoat().get(2),
                new Cell(-1,6),1) == false));
        System.out.println("Boat place once      : " +
                (human.getListBoat().get(2).getPosition().size() == 2));
    }

    public static void placeBoatIATest()
    {
        System.out.println("\n Place boat ia tests :");
        System.out.println("Place boats          : " + (ia.placeBoat(null, null, 0) ==true));
        initGrid();
        char c = 'a';
        for(IBoat b : ia.getListBoat())
        {
            Set key = b.getPosition().keySet();
            Iterator it = key.iterator();
            while(it.hasNext())
            {
                Cell cell = (Cell) it.next();
                grille[cell.getX()][cell.getY()] = c;
            }
            c++;
        }
        displayGrid();
    }

    public static void attackHumanTest()
    {
        System.out.println("\n Attack human tests :");
        human.placeBoat(human.getListBoat().get(0), new Cell(4, 0), 0);
        human.isAttacked(new Cell(4, 0));
        human.isAttacked(new Cell(5, 0));
        System.out.println("Attack               : " +
                            (human.getListBoat().get(0).getPosition().get(new Cell(4, 0))==true));
        System.out.println("Attack               : " +
                (human.getListBoat().get(0).getPosition().get(new Cell(5, 0))==true));
    }

    public static void attackIATest()
    {
        System.out.println("\n Attack ia tests :");
        ia.placeBoat(null, null, 0);
        Cell cell = new Cell(0,0);
        initGrid();
        for(int i = 0 ; i < 81 ; i++)
        {
            ia.addAttack(cell);
            grille[cell.getX()][cell.getY()] = 'o';
        }
        displayGrid();
    }

    private static void initGrid()
    {
        for(int i = 0 ; i < 9 ; i++)
        {
            for(int j = 0 ; j < 9 ; j++)
            {
                grille[i][j] = '.';
            }
        }
    }

    private static void displayGrid()
    {
        for(int i = 0 ; i < 9 ; i++)
        {
            for(int j = 0 ; j < 9 ; j++)
            {
                System.out.print(grille[i][j]);
            }
            System.out.println();
        }
    }

    private static char[][] grille = new char [9][9];
}
