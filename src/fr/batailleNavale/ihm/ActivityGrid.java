package fr.batailleNavale.ihm;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.googlecode.androidannotations.annotations.AfterViews;
import com.googlecode.androidannotations.annotations.Bean;
import com.googlecode.androidannotations.annotations.Click;
import com.googlecode.androidannotations.annotations.EActivity;
import com.googlecode.androidannotations.annotations.InstanceState;
import com.googlecode.androidannotations.annotations.ItemClick;
import com.googlecode.androidannotations.annotations.SystemService;
import com.googlecode.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.EventObject;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import fr.batailleNavale.game.boat.IBoat;
import fr.batailleNavale.game.gameMaster.GameMaster;
import fr.batailleNavale.game.gameMaster.IGameMaster;
import fr.batailleNavale.game.grid.Cell;
import fr.batailleNavale.network.Client;
import fr.batailleNavale.network.Event.EventBadConnectionListener;
import fr.batailleNavale.network.Event.EventClientListener;
import fr.batailleNavale.network.Event.EventGoodConnectionListener;
import fr.batailleNavale.network.Event.EventIPListener;
import fr.batailleNavale.network.Event.EventRecvMesgListener;
import fr.batailleNavale.network.Protocole;
import fr.batailleNavale.network.Server;

/**
 * Correspond aux deux grilles d'un joueur.
 */
@EActivity(R.layout.grid)
    public class ActivityGrid extends Activity implements EventIPListener,
        EventClientListener, EventBadConnectionListener,
        EventGoodConnectionListener, EventRecvMesgListener
    {
	@ViewById TextView infos;
	@ViewById TextView player_name;
	@ViewById Button nextPlayer,changerSensBateau;
	@ViewById GridView mainGrid;
    @ViewById GridView secondGrid;
	@InstanceState
	    Boolean init = false;
	@InstanceState
	    Integer current_player, current_index, dir=0;
	@SystemService
	    Vibrator vibreur;
	@InstanceState
	    String etape, message;
	@Bean ImageAdapter mainAdapter, secondAdapter;
	
	private String type;
	private String nameJ1;
	private String nameJ2;
	static IGameMaster game_master = null;
	static List<IBoat> lst_boat;

    private Server server;
    private Client client;
    private Boolean placeBoat = false;
    private Boolean otherPlaceBoat = false;

	public void onCreate(Bundle savedInstanceState)
	{
	    super.onCreate(savedInstanceState);
	    setContentView(R.layout.grid);

	    Bundle bundle = getIntent().getExtras();
	    this.type = bundle.getString("type");
	    this.nameJ1 = bundle.getString("name1");

	    if( this.type.equals("PVP") )
        {
            this.nameJ2 = bundle.getString("name2");
        }
        else if( this.type.equals("PVM") )
        {
            String network = bundle.getString("network");
            if( network.equals("server") )
            {
                server = new Server(nameJ1, 5555);
                this.server.addEventIPListener(this);
                this.server.addEventClientListener(this);
                this.server.addEventRecvMesgListener(this);
            }
            else if( network.equals("client"))
            {
            	try {
					Thread.currentThread().sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
                client = new Client(bundle.getString("ip"),5555, nameJ1);
                this.client.addEventBadConnectionListener(this);
                this.client.addEventGoodConnectionListener(this);
                this.client.addEventRecvMesgListener(this);
            }
        }
	    else
        {
            Log.i("----------------------Activity Grid-------------------", "Jeu avec IA");
            this.nameJ2 = "IA";
        }


	    init = true;
	    dir = 0;
	}

    /**
     * Initialisation du jeu.
     * Placement des bateaux du premier joueur.
     */
	@AfterViews
    public void InitGame()
	{
	    if( this.game_master == null && this.init && this.type != null)
		{
		    /* Initialisation du jeu
		     * en fonction du type de jeu choisi par l'utilisateur.
		     */
		    if(type.equals("PVP")) game_master = new GameMaster(2, nameJ1, nameJ2, this); // humain vs humain
		    if(type.equals("PVE")) game_master = new GameMaster(1, nameJ1, nameJ2, this); // humain vs IA
            if(type.equals("PVM")) game_master = new GameMaster(3, nameJ1, nameJ2, this); // Network

            /* Le premier joueur à jouer est le joueur 1. */
		    this.current_index = 0;
            this.current_player = 1;
            this.etape = "placement";
		    this.lst_boat = game_master.getPlayerBoatList(1);

            /* Initialisation des zones de texte affiché à l'écran. */
            this.player_name.setText(this.nameJ1);
            this.infos.setText(this.getResources().getString(R.string.place_boat) + " " +
                    Integer.toString(lst_boat.get(0).getSize()));

            /* On enlève le bouton du joueur suivant
             * car le premier joueur n'a pas encore fini de placer tous
             * ses bateaux.
             */
		    nextPlayer.setVisibility(View.INVISIBLE);

		}
	}

    //dessine la gille
	@AfterViews void bindAdapter()
    {
	    this.mainGrid.setAdapter(this.mainAdapter);     // Grille principale.
        this.secondGrid.setAdapter(this.secondAdapter); // Seconde grille.
	}
	
	@Click public void changerSensBateau()
	{
	    if( dir.equals(1) )
		{
		    changerSensBateau.setText(R.string.changerSensVertical);
		    dir = 0;
		}
	    else
		{
		    changerSensBateau.setText(R.string.changerSensHorizontal);
		    dir = 1;
		}
	}
	
	@Click public void nextPlayer()
	{
	    this.nextPlayer.setVisibility(View.INVISIBLE);

        // on place les bateaux
	    if(this.etape.equals("placement"))
        {
            this.changerSensBateau.setVisibility(View.INVISIBLE);
            setBoatPlayer2();
        }

        // premier coup du jeu.
		else if(this.etape.equals("start"))
        {
            this.changerSensBateau.setVisibility(View.INVISIBLE);
            this.secondGrid.setVisibility(View.VISIBLE);
            firstShot();
        }
        else if(this.etape.equals("play") || this.etape.equals("next"))
        {
			if(type.equals("PVP")) switchPlayerDuringGame();
			else
            {
			    this.nextPlayer.setVisibility(View.INVISIBLE);
                this.changerSensBateau.setVisibility(View.INVISIBLE);
			    this.game_master.attack(2, new Cell(0,0)); // on fait jouer l'IA
			    this.lst_boat = game_master.getPlayerBoatList(2);
			    gamePlay();
			}
		}
        if( type.equals("PVM"))
        {
            // Main grid.
            for(int i = 0; i < 81; i++)
            {
                ImageView image = (ImageView) this.mainGrid.getChildAt(i);
                if( image != null ) image.setImageResource(R.drawable.water);
            }
        }
	}

	private void switchPlayerDuringGame()
    {
	    this.lst_boat = game_master.getPlayerBoatList(this.current_player); // bateaux de l'ancien joueur (pour afficher les portions de bateaux touchés)
	    this.current_player = (((this.current_player-1)+1)%2)+1; // changement de joueur
	    gamePlay();
	}
	
	private void gamePlay()
    {
	    redraw();
        changerSensBateau.setVisibility(View.INVISIBLE);
	    this.etape = "play";

        if( !type.equals("PVM"))
        {
            if( this.current_player == 1) this.player_name.setText(this.nameJ1);
            else this.player_name.setText(this.nameJ2);
        }

	    infos.setText(this.getResources().getString(R.string.attaquer));
		
	    int fin = game_master.endGame();

	    if( fin != 0 ) // Le jeu est fini !
        {
            String name;
            if( fin == 1 ) name = this.nameJ1;
            else name = this.nameJ2;

            message = name + " " + this.getResources().getString(R.string.win);
            infos.setText(message);
            etape = "end";
	    }
	}

	private void firstShot()
    {
        if( client == null )
        {
            this.player_name.setText(this.nameJ1);
            this.infos.setText(this.getResources().getString(R.string.attaquer));
            etape = "play";
            current_player = 1;
            lst_boat = new ArrayList<IBoat>();
            redraw();
        }
        else
        {
            this.infos.setText(nameJ2 + " est en train de jouer.");
            lst_boat = new ArrayList<IBoat>();
            redraw();
        }
	}

	private void setBoatPlayer2()
    {
	    if(type.equals("PVP"))
        {
            this.changerSensBateau.setVisibility(View.VISIBLE);
		    current_player = 2;
		    lst_boat = game_master.getPlayerBoatList(2);
		    current_index = 0;
            this.player_name.setText(this.nameJ2);
            this.infos.setText(this.getResources().getString(R.string.place_boat) + " " +
                    Integer.toString(lst_boat.get(0).getSize()));
		    redraw();
	    }
        else if(type.equals("PVE"))
        {
		    game_master.placeBoat(2, null, null, null);
		    etape = "start";
		    nextPlayer.setVisibility(View.VISIBLE);
            this.nextPlayer.setText(this.getResources().getString(R.string.button_start));
	    }
	}

	public void onBackPressed()
    {
	    Log.d("Quit", "quit");
	    game_master = null;
	    finish();
	}

	@ItemClick public void mainGridItemClicked(int position)
    {
	    if( etape == "placement" )
        {
    		Cell coordo = positionToCoordonate(position);
	    	int x = coordo.getX(), y = coordo.getY();
	
		    if(!lst_boat.isEmpty() && lst_boat.size() > current_index )
		    {
                IBoat bateau = lst_boat.get(current_index);
                int taille = bateau.getSize();

                Cell start = new Cell(x, y);

                if(game_master.placeBoat(current_player, bateau, start, dir))
                 {
                    current_index++;
                    nextBoatPlacement();
                }
                else
                {
                    // vibreur + message
                    vibreur.vibrate(1000);
                }
		    }
	    }
        else if(etape == "play") // en jeu
        {
            if( type.equals("PVM"))
            {
                // Envoyer la case attaqué à l'autre et attendre avant de rejouer.
                if( server != null ) server.sendMsg(Protocole.P_CELL + positionToCoordonate(position).toString());
                if( client != null ) client.sendMsg(Protocole.P_CELL + positionToCoordonate(position).toString());
                this.infos.setText(nameJ2 + " est en train de jouer.");
                drawTouchedPart(positionToCoordonate(position));
            }
            else
            {
                touchedOrNot(positionToCoordonate(position));

                nextPlayer.setVisibility(View.VISIBLE);
                nextPlayer.setText(R.string.next_player);
            }

            etape="next"; // next pour éviter que le joueur ne joue plusieurs fois.
            if( type.equals("PVM") ) mainGrid.setClickable(false);
            if( type.equals("PVE")) nextPlayer();
	    }
        else if(etape.equals("end"))
        {
            this.nextPlayer.setVisibility(View.INVISIBLE);
            this.mainGrid.setClickable(false);
        }
	}

    private void touchedOrNot(Cell position)
    {
        MediaPlayer mp = new MediaPlayer();

        String touched;
        if(game_master.attack(current_player,position)){
            mp = MediaPlayer.create(ActivityGrid.this, R.raw.explode4);
            mp.start();
            touched = "Touché !";
            ImageView image = (ImageView) mainGrid.getChildAt(coordonateToPosition(position));
            image.setImageResource(R.drawable.boat_touched);
        }
        else {
            mp = MediaPlayer.create(ActivityGrid.this, R.raw.splash);
            mp.start();
            touched = "Manqué...";
            drawTouchedPart(position);
        }

        Toast toast = Toast.makeText(getApplicationContext(), touched,
                Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
        toast.show();
    }
	
	/**
	 * The grid is redraw, the first step is to put water on all the map. Next boats are drawed.
	 */
     private void redraw()
	{
        if( !type.equals("PVM") || etape.equals("placement") )
        {
            // Main grid.
            for(int i = 0; i < 81; i++)
            {
                ImageView image = (ImageView) this.mainGrid.getChildAt(i);
                if( image != null ) image.setImageResource(R.drawable.water);
            }

            // Second grid.
            for(int i = 0; i < 81; i++)
            {
                ImageView image = (ImageView) this.secondGrid.getChildAt(i);
                if( image != null ) image.setImageResource(R.drawable.water);
            }
            
            // Grille principale.
            for(Cell c: game_master.getPlayerAttackedCell(current_player))
            {
                drawTouchedPart(c);
            }

            // Grille principale.
            List<IBoat> boats = lst_boat;
            int joueur = (type.equals("PVM")) ? 1 : current_player;
            Boolean own = (lst_boat == game_master.getPlayerBoatList(current_player));
            Log.e("player", current_player.toString());
            Log.e("player", own.toString());
            for(IBoat b: boats)
            {
                drawBoat(own, b);
            }

        }

        // Seconde grille.
        int joueur = (type.equals("PVM")) ? 1 : current_player;
        lst_boat = game_master.getPlayerBoatList(joueur);
        for(IBoat b : lst_boat)
        {
            drawBoatSecondGrid(b);
        }
	}

    private void drawBoatSecondGrid(IBoat b)
    {
        HashMap<Cell,Boolean> positions = b.getPosition();
        Iterator<Entry<Cell, Boolean>> it = positions.entrySet().iterator();

        while (it.hasNext())
        {
            Entry<Cell, Boolean> pairs = (Entry<Cell, Boolean>) it.next();
            Cell pos = (Cell) pairs.getKey();
            Boolean touched = (Boolean) pairs.getValue();

            Cell c = new Cell(pos.getX(), pos.getY());
            int posReel = coordonateToPosition(c);
            //Log.d("Part of boat touched", Boolean.toString(touched));
            if(!touched)
            {
                ImageView image = (ImageView) this.secondGrid.getChildAt(posReel);
                if( image != null) image.setImageResource(R.drawable.boat);
            }
            else
            {
                ImageView image = (ImageView) this.secondGrid.getChildAt(posReel);
                if( image != null) image.setImageResource(R.drawable.boat_touched);
            }
        }
    }

	private void drawBoat(Boolean own, IBoat b)
    {
	    HashMap<Cell,Boolean> positions = b.getPosition();

	    Iterator<Entry<Cell, Boolean>> it = positions.entrySet().iterator();

	    while (it.hasNext())
		{
		    Entry<Cell, Boolean> pairs = (Entry<Cell, Boolean>) it.next();
		    Cell pos = (Cell) pairs.getKey();
		    Boolean touched = (Boolean) pairs.getValue();

		    Cell c = new Cell(pos.getX(), pos.getY());
		    int posReel = coordonateToPosition(c);

                if(own){ // on affiche le bateau si c'est le sien ou si c'est une partie de bateau touché
                    ImageView image = (ImageView) mainGrid.getChildAt(posReel);
                    image.setImageResource(R.drawable.boat);
                }else if(touched){
                	ImageView image = (ImageView) mainGrid.getChildAt(posReel);
                    image.setImageResource(R.drawable.boat_touched);
                }

		}
	}
	
	private void drawTouchedPart(Cell c)
    {
	    int posReel = coordonateToPosition(c);
	    ImageView image = (ImageView) mainGrid.getChildAt(posReel);
        image.setImageResource(R.drawable.cross);
	}

	/**
	 * Construction des grilles.
	 */
	@SuppressLint("NewApi") private void buildGrid(int id)
    {
	    GridView grid = (GridView) this.findViewById(id);
	    grid.setAdapter(new ImageAdapter(this));
	}

	private void nextBoatPlacement()
	{
	    redraw();

        nextPlayer.setVisibility(View.INVISIBLE);
	    if( lst_boat.size() == current_index )
		{
		    if(current_player == 1) // Next player.
			{
			    infos.setText(R.string.putEnd);
                changerSensBateau.setVisibility(View.INVISIBLE);

                if( type.equals("PVM")) // On dit à l'adversaire qu'on a fini de placer nos bateaux.
                {
                    nextPlayer.setVisibility(View.INVISIBLE);
                    if( server != null) server.sendMsg(Protocole.P_BOAT);
                    else if( client != null) client.sendMsg(Protocole.P_BOAT);
                    placeBoat = true;
                    if( otherPlaceBoat )
                    {
                        etape = "start";
                        nextPlayer.setVisibility(View.VISIBLE);
                        nextPlayer.setText("Jouer");
                    }
                }
                else nextPlayer.setVisibility(View.VISIBLE);
			}
		    else
			{
			    infos.setText(R.string.putEnd);
                changerSensBateau.setVisibility(View.INVISIBLE);
			    etape = "start";
                nextPlayer.setVisibility(View.VISIBLE);
			}
		}
        else infos.setText(getResources().getString(R.string.place_boat) + " " + Integer.toString(lst_boat.get(current_index).getSize()));
    }
	
	private Cell positionToCoordonate(int position)
    {
	    int x, y;
	    x = position % 9;
	    y = position / 9;
	    Cell c = new Cell(x, y);
	    return c;
	}
	
	private int coordonateToPosition(Cell position)
    {
	    int p = 0;
	    int x = position.getX(), y = position.getY();
		
	    p = y*9+x;
	
	    return p;
	}

    @Override
    public void handleEventRecvMesg(EventObject e)
    {
        Log.d("ActivityGrid---------", "Message reçu.");
        String msg = " ";

        if( server != null)
        {
            msg = server.getMsgRecv();
            if( msg.equals(Protocole.P_PSEUDO)) server.sendMsg(Protocole.P_PSEUDO+ " "+server.getServerName());
            else if( msg.matches(Protocole.P_PSEUDO + "(.*)") ) // On reçoie le pseudo du client.
            {
                nameJ2 = msg.substring(Protocole.P_PSEUDO.length(),msg.length());
            }
            else if( msg.equals(Protocole.P_BOAT) ) // Le client à fini de placer ses bateaux.
            {
                if( placeBoat )
                {
                    etape = "start";
                    nextPlayer.setVisibility(View.VISIBLE);
                    nextPlayer.setText("Jouer");
                }
                otherPlaceBoat = true;
            }
            else if( msg.matches(Protocole.P_CELL + "(.*)")) // On reçoie une l'attaque du client
            {
                if( game_master.attack(2,stringToPosition(msg, Protocole.P_CELL.length()) ) )
                    server.sendMsg(Protocole.P_TOUCHED + stringToPosition(msg, Protocole.P_CELL.length()).toString());
                redraw();
                if( game_master.getPlayer(1).win() )
                {
                    server.sendMsg(Protocole.P_LOSE);
                    message = nameJ2 + " " + this.getResources().getString(R.string.win);
                    infos.setText(message);
                    etape = "end";
                }
                else
                {
                    etape="play";
                    mainGrid.setClickable(true);
                    infos.setText(this.getResources().getString(R.string.attaquer));
                }
            }
            else if( msg.matches(Protocole.P_TOUCHED + "(.*)")) // On reçoie du client si on a touché ou non son bateau.
            {
                ImageView image = (ImageView) this.mainGrid.getChildAt(coordonateToPosition(
                                                                         stringToPosition(msg, Protocole.P_TOUCHED.length())));
                if( image != null) image.setImageResource(R.drawable.boat);
            }
            else if( msg.equals(Protocole.P_LOSE)) // Le server à perdu.
            {
                message = nameJ1 + " " + this.getResources().getString(R.string.win);
                infos.setText(message);
                etape = "end";
            }
        }
        else if( client != null)
        {
            msg = client.getMsgRecv();
            if( msg.equals(Protocole.P_PSEUDO)) client.sendMsg(Protocole.P_PSEUDO+ " "+client.getClientName());
            else if( msg.matches(Protocole.P_PSEUDO + "(.*)") ) // On reçoie le pseudo du server.
            {
                nameJ2 = msg.substring(Protocole.P_PSEUDO.length(),msg.length());
            }
            else if( msg.matches(Protocole.P_BOAT) ) // Le server à fini de placer ses bateaux.
            {
                if( placeBoat )
                {
                    etape = "start";
                    nextPlayer.setVisibility(View.VISIBLE);
                    nextPlayer.setText("Jouer");
                }
                otherPlaceBoat = true;
            }
            else if( msg.matches(Protocole.P_CELL + "(.*)")) // On reçoie une l'attaque du server
            {
                if( game_master.attack(2,stringToPosition(msg, Protocole.P_CELL.length()) ) )
                    client.sendMsg(Protocole.P_TOUCHED + stringToPosition(msg, Protocole.P_CELL.length()).toString());
                redraw();
                if( game_master.getPlayer(1).win() )
                {
                    client.sendMsg(Protocole.P_LOSE);
                    message = nameJ2 + " " + this.getResources().getString(R.string.win);
                    infos.setText(message);
                    etape = "end";
                }
                else
                {
                    etape="play";
                    mainGrid.setClickable(true);
                    infos.setText(this.getResources().getString(R.string.attaquer));
                }
            }
            else if( msg.matches(Protocole.P_TOUCHED + "(.*)")) // On reçoie du server si on a touché ou non son bateau.
            {
                ImageView image = (ImageView) this.mainGrid.getChildAt(coordonateToPosition(
                        stringToPosition(msg, Protocole.P_TOUCHED.length())));
                if( image != null) image.setImageResource(R.drawable.boat);
            }
            else if( msg.equals(Protocole.P_LOSE)) // Le server à perdu.
            {
                message = nameJ1 + " " + this.getResources().getString(R.string.win);
                infos.setText(message);
                etape = "end";
            }
        }
    }

    private Cell stringToPosition(String pos, int taille)
    {
        Cell cell;
        String coor = pos.substring(taille, pos.length());
        String Y = coor.substring(2, 3);
        String X = coor.substring(0, 1);
        cell = new Cell(Integer.parseInt(X), Integer.parseInt(Y));
        return cell;
    }

    @Override
    public void handleEventBadConnection(EventObject e)
    {
        finish();
    }

    @Override
    public void handleEventGoodConnection(EventObject e)
    {
        if( client != null ) client.sendMsg(Protocole.P_PSEUDO);
    }

    @Override
    public void handleEventClient(EventObject e)
    {
        if( server != null ) server.sendMsg(Protocole.P_PSEUDO);
    }

    @Override
    public void handleEventIp(EventObject e) {

    }
}
