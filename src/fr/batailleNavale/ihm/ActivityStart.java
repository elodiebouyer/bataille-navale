package fr.batailleNavale.ihm;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.googlecode.androidannotations.annotations.EActivity;

import java.util.EventObject;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import fr.batailleNavale.ihm.fragments.HeadlinesFragment;
import fr.batailleNavale.ihm.fragments.NetworkFragment;
import fr.batailleNavale.ihm.fragments.OnePlayerFragment;
import fr.batailleNavale.ihm.fragments.ScoresFragment;
import fr.batailleNavale.ihm.fragments.TwoPlayerFragment;
import fr.batailleNavale.network.Client;
import fr.batailleNavale.network.Event.EventClientListener;
import fr.batailleNavale.network.Event.EventGoodConnectionListener;
import fr.batailleNavale.network.Event.EventBadConnectionListener;
import fr.batailleNavale.network.Event.EventIPListener;
import fr.batailleNavale.network.Event.EventRecvMesgListener;
import fr.batailleNavale.network.Server;
import fr.batailleNavale.stats.DataBase;
import fr.batailleNavale.stats.ExchangeBDD;
import fr.batailleNavale.stats.RecapPlayer;
import fr.batailleNavale.stats.RecapPlayerBDD;


@EActivity(R.layout.start)
public class ActivityStart extends FragmentActivity
        implements HeadlinesFragment.OnHeadlineSelectedListener,
                   EventIPListener, EventClientListener, EventBadConnectionListener,
                   EventGoodConnectionListener, EventRecvMesgListener
{

    int mPosition = 0;
    private Server server = null;
    private Client client = null;
    private String nameClient;
    private TextView ipAddress;
    private String ipAddr;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.start);

        if (savedInstanceState != null)
        {
            return;
        }

        HeadlinesFragment firstFragment = new HeadlinesFragment();
        firstFragment.setArguments(getIntent().getExtras());

        if( findViewById(R.id.fragment_container) == null)
                getSupportFragmentManager().beginTransaction().add(R.id.headlines_fragment, firstFragment).commit();
        else getSupportFragmentManager().beginTransaction().add(R.id.fragment_container, firstFragment).commit();
    }

    public void onTypeGameSelected(int position)
    {
        this.mPosition = position;
        Fragment frag = null;

        switch (position)
        {
            // Player vs Player.
            case 0:
                frag = new TwoPlayerFragment();
                break;

            // Player vs IA.
            case 1:
                frag = new OnePlayerFragment();
                break;

            // Player vs player in network.
            case 2:
            	if(isConnectedWifi())
            		frag = new NetworkFragment();
            	else{
            		Toast toast = Toast.makeText(getApplicationContext(), "Connectez vous au WiFi",
                            Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                    toast.show();
            		return;
            	}
                break;

            // Scores page.
            case 3:
                frag = new ScoresFragment();
                break;

            // Exit.
            case 4:
                finish();
                return;

            default:
                frag = new OnePlayerFragment();
        }

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        if( findViewById(R.id.fragment_container) == null ) transaction.replace(R.id.player_fragment, frag);
        else transaction.replace(R.id.fragment_container, frag);
        transaction.commit();
    }

    /**
     * Called after click in the button to start the server.
     * @param v
     */
    public void server(View v)
    {
        client = null;
        EditText name = (EditText) findViewById(R.id.textNom);
        TextView adress = (TextView) findViewById(R.id.adress);
        ipAddress = (TextView) findViewById(R.id.ipAddress);
        TextView label = (TextView) findViewById(R.id.label);
        TextView hote = (TextView) findViewById(R.id.ipHote);
        Button connect = (Button) findViewById(R.id.buttonConnect);

        if (name.getText().toString().isEmpty())
        {
            name.setError(getResources().getString(R.string.name_error));
            adress.setVisibility(View.INVISIBLE);
            ipAddress.setVisibility(View.INVISIBLE);
            label.setVisibility(View.GONE);
            hote.setVisibility(View.GONE);
            connect.setVisibility(View.GONE);
            this.server = null;
            return;
        }

        if ( this.server == null )
        {
            this.server = new Server(name.getText().toString(), 4444);
            this.server.addEventIPListener(this);
            this.server.addEventClientListener(this);
            this.server.addEventRecvMesgListener(this);
        }

        adress.setVisibility(View.VISIBLE);
        label.setVisibility(View.GONE);
        hote.setVisibility(View.GONE);
        connect.setVisibility(View.GONE);
        ipAddress.setVisibility(View.VISIBLE);
        ipAddress.setText(server.getipAddress());
    }

    @Override
    public void handleEventIp(EventObject e)
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ipAddress.setText(server.getipAddress());
            }
        });
    }

    /**
     * Called after click in the button to start the client.
     * @param v
     */
    public void client(View v)
    {
        server = null;
        TextView ipAddress = (TextView) findViewById(R.id.ipAddress);
        ipAddress.setVisibility(View.GONE);

        TextView adress = (TextView) findViewById(R.id.adress);
        adress.setVisibility(View.GONE);

        TextView label = (TextView) findViewById(R.id.label);
        label.setVisibility(View.VISIBLE);

        TextView hote = (TextView) findViewById(R.id.ipHote);
        hote.setVisibility(View.VISIBLE);

        Button connect = (Button) findViewById(R.id.buttonConnect);
        connect.setVisibility(View.VISIBLE);
    }

    /**
     * Call by the server when a opponent is connected.
     * @param e
     */
    @Override
    public void handleEventClient(EventObject e)
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run()
            {
                EditText name = (EditText) findViewById(R.id.textNom);
                openActivity(ActivityGrid_.class, "type", "PVM",
                        "name1", name.getText().toString(),
                        "name2", " ", "network", "server");
            }
        });
    }

    /**
     * Call by the client.
     * @param e
     */
    @Override
    public void handleEventGoodConnection(EventObject e)
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run()
            {
                EditText name = (EditText) findViewById(R.id.textNom);
                openActivity(ActivityGrid_.class, "type", "PVM",
                        "name1", name.getText().toString(),
                        "name2", " ", "network", "client");
            }
        });
    }

    public void connexion(View v)
    {
        // Test du nom.
        EditText name = (EditText) this.findViewById(R.id.textNom);
        if (name.getText().toString().isEmpty())
        {
            name.setError(this.getResources().getString(R.string.name_error));
            return;
        }

        // Test de l'ip.
        EditText ip = (EditText) this.findViewById(R.id.ipHote);
        if (ip.getText().toString().isEmpty())
        {
            ip.setError(this.getResources().getString(R.string.error_ip));
            return;
        }

        // Connection.
        this.client = new Client(ip.getText().toString(),4444, name.getText().toString());
        this.client.addEventBadConnectionListener(this);
        this.client.addEventGoodConnectionListener(this);
        this.client.addEventRecvMesgListener(this);
    }

    @Override
    public void handleEventBadConnection(EventObject e)
    {
        // Status de la connection.
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                EditText ip = (EditText) findViewById(R.id.ipHote);
                ip.setError(getResources().getString(R.string.error_ip));
            }
        });
    }

    @Override
    public void handleEventRecvMesg(EventObject e)
    {

    }

    /**
     * Called after click in the button "Jouer".
     * @param v
     */
     public void startGame(View v)
     {
         EditText name1 = (EditText) this.findViewById(R.id.text1);
         EditText name2 = (EditText) this.findViewById(R.id.text2);

         if (name1.getText().toString().isEmpty())
         {
             name1.setError(this.getResources().getString(R.string.name_error));
             return;
         }
         else if( name2 != null && name2.getText().toString().isEmpty())
         {
             name2.setError(this.getResources().getString(R.string.name_error));
             return;
         }

         switch (this.mPosition)
         {
             case 0:
                 openActivity(ActivityGrid_.class, "type", "PVP",
                                                   "name1", name1.getText().toString(),
                                                   "name2", name2.getText().toString(),
                                                    null, null);
                 break;

             case 1:;
                 openActivity(ActivityGrid_.class, "type", "PVE",
                                                    "name1", name1.getText().toString(),
                                                    null, null, null, null);

         }
     }

    private <T> void openActivity(Class<T> cls, String keyType, String type,
                                  String key1, String name1,
                                  String key2, String name2,
                                  String key3, String network)
    {
        Intent intent = new Intent(this, cls);
        intent.putExtra(keyType, type);
        intent.putExtra(key1, name1);
        if( key2 != null ) intent.putExtra(key2, name2);
        if( key3 != null )
        {
            intent.putExtra(key3, network);
            EditText ip = (EditText) this.findViewById(R.id.ipHote);
            if( client != null ) intent.putExtra("ip", ip.getText().toString());
        }
       // server.stop();
        server = null;
        client = null;
        startActivity(intent);
    }


    private <T> void openActivity(Class<T> cls, String keyType, String type,
                                                String keyName1, String name1)
    {
        openActivity(cls, keyType, type, keyName1, name1, null, null, null, null);
    }
    

 public void deleteScore(View v){
    	RecapPlayerBDD playerBdd = new RecapPlayerBDD(v.getContext());
    	DataBase getBDD = playerBdd.getDataBase();
        playerBdd.open();
        getBDD.delete(playerBdd.getBDD());
        playerBdd.close();
 }

    private Boolean isConnectedWifi(){
    	WifiManager wifiManager = (WifiManager) this.getSystemService(Context.WIFI_SERVICE); 
    	
    	ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
    	NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

    	if(!wifiManager.isWifiEnabled()){
    		wifiManager.setWifiEnabled(true);
    	}
    	
    	return mWifi.isConnected();

    }
}
