package fr.batailleNavale.ihm.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import fr.batailleNavale.ihm.R;

/**
 * Created by elodie on 09/12/13.
 */
public class HeadlinesFragment extends ListFragment
{
    OnHeadlineSelectedListener mCallback;
    static String TYPE_GAME[]= null;

    public interface OnHeadlineSelectedListener
    {
        public void onTypeGameSelected(int position);
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        this.TYPE_GAME = new String[5];
        this.TYPE_GAME[0] = this.getActivity().getString(R.string.menu_start_new_game_pvp);
        this.TYPE_GAME[1] = this.getActivity().getString(R.string.menu_start_new_game_pve);
        this.TYPE_GAME[2] = this.getActivity().getString(R.string.menu_start_new_game_pvm);
        this.TYPE_GAME[3] = this.getActivity().getString(R.string.menu_show_stats);
        this.TYPE_GAME[4] = this.getActivity().getString(R.string.exit);

        setListAdapter(new ArrayAdapter<String>(getActivity(), R.layout.fragment_headlines, this.TYPE_GAME));
    }

    @Override
    public void onStart() {
        super.onStart();

        if (getFragmentManager().findFragmentById(R.id.fragment_name) != null) {
            getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        }
    }

    @Override
    public void onAttach(Activity activity)
    {
        super.onAttach(activity);

        try
        {
            mCallback = (OnHeadlineSelectedListener) activity;
        }
        catch (ClassCastException e)
        {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id)
    {
        mCallback.onTypeGameSelected(position);
        getListView().setItemChecked(position, true);
    }
}
