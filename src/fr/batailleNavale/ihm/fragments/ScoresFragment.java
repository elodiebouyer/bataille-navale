package fr.batailleNavale.ihm.fragments;


import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import java.util.Iterator;
import java.util.List;

import fr.batailleNavale.ihm.R;
import fr.batailleNavale.stats.*;


/**
 * Created by elodie on 15/12/13.
 */
public class ScoresFragment extends Fragment
{
	private LinearLayout view;
	
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        this.view = (LinearLayout) inflater.inflate(R.layout.fragment_scores, container, false);

        RecapPlayerBDD playerBdd = new RecapPlayerBDD(view.getContext());
        ExchangeBDD connection = new ExchangeBDD();

        List<RecapPlayer> listRecap = connection.getAllBasicScore(playerBdd);

        Iterator<RecapPlayer> it = listRecap.iterator();
        RecapPlayer recap;

        TableLayout tableDyna = (TableLayout) view.findViewById(R.id.dynamicLayout);
        TableRow ligneDyna = new TableRow(view.getContext());

        TableRow.LayoutParams layoutParams = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);

        ligneDyna.addView(generateTextView(this.getResources().getString(R.string.S_number), layoutParams));
        ligneDyna.addView(generateTextView(this.getResources().getString(R.string.S_name), layoutParams));
        ligneDyna.addView(generateTextView(this.getResources().getString(R.string.S_number_win), layoutParams));
        ligneDyna.addView(generateTextView(this.getResources().getString(R.string.S_total_game), layoutParams));
        ligneDyna.addView(generateTextView(this.getResources().getString(R.string.S_total_move), layoutParams));
        ligneDyna.addView(generateTextView(this.getResources().getString(R.string.S_pourcentage), layoutParams));
        tableDyna.addView(ligneDyna, layoutParams);

        if( listRecap.isEmpty()) return view;


        for (int i = 0; i < listRecap.size(); i++) {
            recap = it.next();
            ligneDyna = new TableRow(this.getActivity());
            ligneDyna.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT,
                        TableRow.LayoutParams.WRAP_CONTENT));

            ligneDyna.addView(generateTextView(String.valueOf(i+1), layoutParams));
            ligneDyna.addView(generateTextView(recap.getName(), layoutParams));
            ligneDyna.addView(generateTextView(String.valueOf(recap.getWin()), layoutParams));
            ligneDyna.addView(generateTextView(String.valueOf(recap.getSumGame()), layoutParams));
            ligneDyna.addView(generateTextView(String.valueOf(recap.getMove()), layoutParams));
            ligneDyna.addView(generateTextView(String.valueOf(recap.getWin()*100/recap.getSumGame()), layoutParams));
            tableDyna.addView(ligneDyna, layoutParams);
        }
        return view;
    }

    public TextView generateTextView(String texte, TableRow.LayoutParams params)
    {
        TextView result = new TextView(this.getActivity());

        result.setText(texte);
        result.setTextColor(Color.rgb(0,48,145));
        result.setTextSize(18);
        result.setLayoutParams(params);

        return result;
    }
    

    @Override
    public void onStart()
    {
        super.onStart();
    }

    @Override
    public void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
    }
}
