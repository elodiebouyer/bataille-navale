package fr.batailleNavale.game.grid;

/**
 * Created by elodie on 27/10/13.
 */
public class Cell
{
    private int X;
    private int Y;

    public Cell(int x, int y)
    {
        this.X = x;
        this.Y = y;
    }

    public final int getX()
    {
        return X;
    }

    public final int getY()
    {
        return Y;
    }

    public final boolean inGrid()
    {
        return (this.getX() >= 0 &&
                           this.getX() < 9  &&
                           this.getY() >= 0 &&
                           this.getY() < 9);
    }

    public void setX(int _x)
    {
        this.X = _x;
    }

    public void setY(int _y)
    {
        this.Y = _y;
    }

    @Override
    public boolean equals(Object c)
    {
        if( c == this) return true;

        if( c instanceof Cell )
        {
            Cell other = (Cell) c;

            if( this.X != other.getX()) return false;
            if( this.Y != other.getY()) return false;

            return true;
        }
        return false;
    }

    @Override
    public String toString()
    {
        return  X + " " + Y;
    }

    @Override
    public int hashCode() {
        return X+Y *10 ;
    }
}
