package fr.batailleNavale.game.boat;

import java.util.HashMap;

import fr.batailleNavale.game.grid.Cell;

/**
 * Created by elodie on 23/10/13.
 */
public class Battleship extends Boat
{
    public Battleship()
    {
        this.m_size = 4;
        this.m_position = new HashMap<Cell, Boolean>();
        this.m_sank = false;
    }
}
