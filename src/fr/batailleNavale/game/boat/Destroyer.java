package fr.batailleNavale.game.boat;

import java.util.HashMap;

import fr.batailleNavale.game.grid.Cell;

/**
 * Created by elodie on 23/10/13.
 */
public class Destroyer extends Boat
{

    public Destroyer()
    {
        this.m_size = 2;
        this.m_position = new HashMap<Cell, Boolean>();
        this.m_sank = false;
    }

}
