package fr.batailleNavale.game.boat;

import java.util.HashMap;

import fr.batailleNavale.game.grid.Cell;

/**
 * Created by elodie on 23/10/13.
 */
public interface IBoat
{
    /**
     * Get the boat size.
     * @return The boat size.
     */
    int getSize();

    /**
     * To know if the boat is sank.
     * @return True if is sank, false otherwise.
     */
    boolean isSank();

    /**
     * Assigns the boat a location in the grid.
     * @param _start First cell in the grid.
     * @param _dir Orientation of the boat (0 = line, 1 = column).
     */
    void placeBoat(Cell _start, Integer _dir);

    /**
     * Return true if the boat is in the cell.
     * @param _cell Cell Cell.
     * @return true or false.
     */
    boolean isInCell(Cell _cell);

    /**
     * Part of a boat is touched.
     * @param _cell Cell oordinate.
     */
    void partIsTouch(Cell _cell);


    /**
     * Get boat position.
     * @return position.
     */
    HashMap<Cell,Boolean> getPosition();
}
