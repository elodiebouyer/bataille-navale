package fr.batailleNavale.game.boat;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import fr.batailleNavale.game.grid.Cell;

/**
 * Created by elodie on 25/10/13.
 */
public abstract class Boat implements IBoat
{
    protected HashMap<Cell,Boolean> m_position;
    protected boolean m_sank;
    protected String m_picture; // Path/name to the picture.
    protected int m_size;

    public final int getSize()
    {
        return this.m_size;
    }

    public final HashMap<Cell,Boolean> getPosition()
    {
        return this.m_position;
    }

    public final boolean isSank()
    {
        return this.m_sank;
    }

    public void partIsTouch(Cell _cell)
    {
        this.m_position.put(_cell,true);
        sank();
    }

    public final boolean isInCell(Cell _cell)
    {
        Set key = this.m_position.keySet();
        Iterator it = key.iterator();

        while (it.hasNext())
        {
            Cell c = (Cell) it.next();
            if( c.equals(_cell))
            {
                return true;
            }
        }
        return false;
    }

    public void placeBoat(Cell _start, Integer _dir)
    {
        // If the boat is already placed => delete old positions.
        if( !this.m_position.isEmpty())
        {
            this.m_position.clear();
        }

        if( _dir == 0) // same line.
        {
            for( int i = _start.getX() ; i < _start.getX()+this.m_size ; i++)
            {
                Cell _cell = new Cell(i, _start.getY());
                this.m_position.put(_cell,false);
            }
        }
        else // Same colonne.
        {
            for( int i = _start.getY() ; i < _start.getY()+this.m_size ; i++)
            {
                Cell _cell = new Cell(_start.getX(), i);
                this.m_position.put(_cell,false);
            }
        }
    }

    private void sank()
    {
        for(Boolean b : this.m_position.values())
        {
            if( b == false) return;
        }
        this.m_sank = true;
    }

}
