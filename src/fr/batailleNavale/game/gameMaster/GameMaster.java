package fr.batailleNavale.game.gameMaster;

import android.content.Context;

import java.util.List;

import fr.batailleNavale.game.boat.IBoat;
import fr.batailleNavale.game.grid.Cell;
import fr.batailleNavale.game.players.IPlayer;
import fr.batailleNavale.game.players.PlayerHuman;
import fr.batailleNavale.game.players.PlayerIA;
import fr.batailleNavale.game.players.PlayerNetwork;
import fr.batailleNavale.stats.ExchangeBDD;
import fr.batailleNavale.stats.RecapPlayerBDD;

/**
 * Created by elodie on 23/10/13.
 */
public class GameMaster implements IGameMaster
{
    private IPlayer m_player1= null;
    private IPlayer m_player2 = null;
    private RecapPlayerBDD playerBdd;
    private ExchangeBDD connection;

    /**
     * Creates two human players or one human player and one IA player.
     * @param num : Between 1, 2 or 3 : if num = 2 -> two players ; if num = 1 -> IA + human ; if num = 3 -> réseau.
     */
    public GameMaster(int num, String _name1, String _name2, Context context)
    {
        this.playerBdd = new RecapPlayerBDD(context);
        this.connection = new ExchangeBDD();

        switch (num)
        {
            case 1:
                this.m_player1 = new PlayerHuman(_name1);
                this.m_player2 = new PlayerIA("IA");
                break;

            case 2:
                this.m_player1 = new PlayerHuman(_name1);
                this.m_player2 = new PlayerHuman(_name2);
                break;

            case 3:
                this.m_player1 = new PlayerNetwork(_name1);
                this.m_player2 = new PlayerNetwork(_name2);
                break;

            case 4: // Pour les testes ;)
                this.m_player1 = new PlayerIA("Joueur 1");
                this.m_player2 = new PlayerIA("Joueur 2");
                break;

            default:
                this.m_player1 = new PlayerHuman(_name1);
                this.m_player2 = new PlayerIA("IA");
        }
    }

    public boolean placeBoat(int _player, IBoat _boat, Cell _start, Integer _dir)
    {
        switch (_player)
        {
            case 1:
                return this.m_player1.placeBoat(_boat,_start, _dir);

            case 2:
                return this.m_player2.placeBoat(_boat,_start, _dir);

            default:
                return false;
        }
    }

    public boolean attack(int _player, Cell _cell)
    {
        boolean touched = false;
        switch (_player)
        {
            case 1:
                this.m_player1.addAttack(_cell);
                touched = this.m_player2.isAttacked(_cell);
                this.m_player1.setJustTouched(touched);// Used to IA.
                return touched;

            case 2:
                this.m_player2.addAttack(_cell);
                touched = this.m_player1.isAttacked(_cell);
                this.m_player2.setJustTouched(touched);// Used to IA
                return touched;

            default:
                return false;
        }
    }

    public IPlayer getPlayer(int i)
    {
        if(i == 1) return m_player1;
        else if(i == 2) return m_player2;
        return null;
    }

    public int endGame()
    {
        if( this.m_player1.win() )
        {
            connection.UpdatePLayerScore(this.m_player1, playerBdd);
            connection.UpdatePLayerScore(this.m_player2, playerBdd);
            return 2;
        }
        else if( this.m_player2.win())
        {
            connection.UpdatePLayerScore(this.m_player1, playerBdd);
            connection.UpdatePLayerScore(this.m_player2, playerBdd);
            return 1;
        }
        return 0;
    }

    public List<IBoat> getPlayerBoatList(int _player)
    {
        switch (_player)
        {
            case 1:
                return this.m_player1.getListBoat();

            case 2:
                return this.m_player2.getListBoat();

            default:
                return null;
        }
    }

    public List<Cell> getPlayerAttackedCell(int _player)
    {
        switch (_player)
        {
            case 1:
                return this.m_player1.getAllCellsAttacked();
            case 2:
                return this.m_player2.getAllCellsAttacked();
            default:
                return null;
        }
    }

    public String getPlayerPseudo(int _player)
    {
        switch (_player)
        {
            case 1:
                return this.m_player1.getName();

            case 2:
                return this.m_player2.getName();

            default:
                return "unknown";
        }
    }

    public void setPlayerBoatList(int _player, List<IBoat> list)
    {
        if( _player == 1)
        {
            this.m_player1.setListBoat(list);
        }
        else if( _player == 2)
        {
            this.m_player2.setListBoat(list);
        }
    }
}