package fr.batailleNavale.game.gameMaster;

import java.util.List;

import fr.batailleNavale.game.boat.IBoat;
import fr.batailleNavale.game.grid.Cell;
import fr.batailleNavale.game.players.IPlayer;

/**
 * Created by elodie on 23/10/13.
 */
public interface IGameMaster
{
    /**
     * One player put one boat in the grid.
     * @param _player Numero of the player.
     * @param _boat Boat to put in the grid.
     * @param _start Cell of the first cell.
     * @param _dir 0 for line, 1 for column.
     * @return True if the player placed the boat.
     */
    boolean placeBoat(int _player, IBoat _boat, Cell _start, Integer _dir);

    /**
     * A player attack the other player.
     * @param _player Numero of player who attack.
     * @param _cell Cell X of the cell (null for IA).
     * @return True if is a successful attack.
     */
    boolean attack(int _player, Cell _cell);

    /**
     * Test the end of the game.
     * @return 0 if the game not finish, winner player otherwise (1 or 2).
     */
    int endGame();

    /**
     * Get player boats list.
     * @return Boat list.
     */
    List<IBoat> getPlayerBoatList(int _player);

    /**
     * Get player attacked cell list..
     * @param _player : Player's numero.
     * @return List cells attacked.
     */
    List<Cell> getPlayerAttackedCell(int _player);

    /**
     * Get the player pseudo.
     * @param _player Numero of player.
     * @return Player pseudo.
     */
    String getPlayerPseudo(int _player);

    void setPlayerBoatList(int _player, List<IBoat> list);

    public IPlayer getPlayer(int i);
}
