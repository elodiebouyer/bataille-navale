package fr.batailleNavale.game.players;

import fr.batailleNavale.game.boat.IBoat;
import fr.batailleNavale.game.grid.Cell;

/**
 * Created by elodie on 23/10/13.
 */
public class PlayerHuman extends Player
{

    public PlayerHuman(String _name)
    {
        super();
        this.m_name = _name;
    }

    // _dir 0 for line, 1 for column.
    public boolean placeBoat(IBoat _boat, Cell _cell, Integer _dir)
    {
        Cell _end = cellEnd(_cell,_dir,_boat.getSize()-1);
        if( this.m_listBoat.contains(_boat) &&
            _cell.inGrid() &&
            _end.inGrid() &&
            cellIsFree(_cell,_boat.getSize(), _dir)
          )
        {
            // Add boat.
            this.m_listBoat.get(this.m_listBoat.indexOf(_boat)).placeBoat(_cell, _dir);

            return true;
        }
        return false;
    }

    public void addAttack(Cell _cell)
    {
        if( _cell.inGrid() )
        {
            Cell c = new Cell(_cell.getX(), _cell.getY());
            this.m_listCellAlreadyAttacked.add(c);
        }
    }
}
