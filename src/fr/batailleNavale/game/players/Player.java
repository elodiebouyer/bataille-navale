package fr.batailleNavale.game.players;

import java.util.ArrayList;
import java.util.List;

import fr.batailleNavale.game.boat.AircraftCarrier;
import fr.batailleNavale.game.boat.Battleship;
import fr.batailleNavale.game.boat.Cruiser;
import fr.batailleNavale.game.boat.Destroyer;
import fr.batailleNavale.game.boat.IBoat;
import fr.batailleNavale.game.boat.Submarine;
import fr.batailleNavale.game.grid.Cell;

/**
 * Created by elodie on 23/10/13.
 */
public abstract class Player implements IPlayer
{
    protected List<IBoat> m_listBoat; // Liste des bateaux du joueur.
    protected List<Cell> m_listCellAlreadyAttacked; // Liste des cases où le joueur à déjà attaqué.
    protected String m_name; // Pseudo du joueur.
    protected boolean justTouched;

    public Player()
    {
        this.justTouched = false;

        this.m_listBoat = new ArrayList<IBoat>();

        this.m_listBoat.add(new AircraftCarrier()); // Porte avion (5).
        this.m_listBoat.add(new Cruiser()); // Croiseur (3).
        this.m_listBoat.add(new Destroyer()); // Contre-torpilleur (2).
        this.m_listBoat.add(new Submarine()); // Sous-marin (3).
        this.m_listBoat.add(new Battleship()); // Torpilleur (4).

        this.m_listCellAlreadyAttacked = new ArrayList<Cell>();
    }

    public void setJustTouched(boolean t)
    {
        this.justTouched = t;
    }

    public String getName()
    {
        return this.m_name;
    }

    public List<IBoat> getListBoat()
    {
        return this.m_listBoat;
    }

    public List<Cell> getAllCellsAttacked()
    {
        return  this.m_listCellAlreadyAttacked;
    }

    public boolean win()
    {
        for(IBoat b : this.m_listBoat)
        {
            if( !b.isSank())
            {
                return false;
            }
        }
        return true;
    }

    public boolean isAttacked(Cell _cell)
    {
        for(IBoat b : this.m_listBoat)
        {
            if( b.isInCell(_cell))
            {
                b.partIsTouch(_cell);
                return true;
            }
        }
        return false;
    }

    public abstract void addAttack(Cell _cell);

    // test if the cell is free.
    // _dir 0 for line, 1 for column.
    protected boolean cellIsFree(Cell _cell, int _size, int _dir)
    {
        for(IBoat b : this.m_listBoat)
        {
            if( _dir == 0) // same line.
            {
                for( int i = _cell.getX() ; i < _cell.getX()+_size ; i++)
                {
                    Cell c = new Cell(i,_cell.getY());
                    if(b.isInCell(c))return false;
                }
            }
            else // Same colonne.
            {
                for( int i = _cell.getY() ; i < _cell.getY()+_size ; i++)
                {
                    Cell c = new Cell(_cell.getX(), i);
                    if(b.isInCell(c)) return false;
                }
            }
        }
        return true;
    }

    protected Cell cellEnd(Cell _cell, int _dir, int _size)
    {
        Cell _end;
        if(_dir == 0) _end = new Cell(_cell.getX()+_size,_cell.getY());
        else _end = new Cell(_cell.getX(),_cell.getY()+_size);
        return _end;
    }

    public void setListBoat(List<IBoat> list)
    {
        this.m_listBoat = list;
    }
}
