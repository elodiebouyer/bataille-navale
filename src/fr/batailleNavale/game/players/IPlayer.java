package fr.batailleNavale.game.players;

import java.util.List;

import fr.batailleNavale.game.boat.IBoat;
import fr.batailleNavale.game.grid.Cell;

/**
 * Created by elodie on 23/10/13.
 */
public interface IPlayer
{
    /**
     * Get the attacked cells list.
     * @return Attacked cells list
     */
    List<Cell> getAllCellsAttacked();

    /**
     * Get player name.
     * @return pseudo.
     */
    String getName();

    /**
     * Get the player boats list.
     * @return Boats list.
     */
    List<IBoat> getListBoat();

    /**
     * The Human player put one boat in the grid.
     * The IA player put all boat in the grid.
     * @param _boat Name of the boat.
     * @param _start Cell of the first cell.
     * @param _dir 0 for line, 1 for column.
     * @return True if the player placed the boat.
     */
    boolean placeBoat(IBoat _boat, Cell _start, Integer _dir);

    /**
     * The player is attacked.
     * @param _cell Cell of the cell. (null for IA)
     * @return true if is a successful attack, false otherwise.
     */
    boolean isAttacked(Cell _cell);

    /**
     *
     * @return true if the player win, false otherwise.
     */
    boolean win();

    void addAttack(Cell _cell);

    void setJustTouched(boolean t);

    void setListBoat(List<IBoat> list);
}
