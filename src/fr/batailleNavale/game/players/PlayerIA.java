package fr.batailleNavale.game.players;

import java.util.ArrayList;
import java.util.List;

import fr.batailleNavale.game.boat.IBoat;
import fr.batailleNavale.game.grid.Cell;

/**
 * Created by elodie on 23/10/13.
 */
public class PlayerIA extends Player
{
    private List<Cell> m_possibleAttack;
    private List<Cell> m_maybeBoatHere;

    public PlayerIA(String _name)
    {
        super();
        this.m_name = _name;
        this.m_possibleAttack = new ArrayList<Cell>();
        this.m_possibleAttack.add(firstAttack());
        this.m_maybeBoatHere = new ArrayList<Cell>();
    }

    // boat = null, start = null, end = null
    // On place tous les bateaux en une fois.
    public boolean placeBoat(IBoat boat, Cell _start, Integer _dir)
    {
        _start = new Cell(0,0);
        Cell _end = new Cell(0,0);

        for(IBoat b : this.m_listBoat)
        {
            if( b.getPosition().isEmpty() ) // Not placed.
            {
                // dir = 0 = horizontal ; 1 = vertical.
                do
                {
                    _start.setX( (int)(Math.random() * 8));
                    _start.setY( (int)(Math.random() * 8));
                    _dir = _start.getX() %2;

                    if(_dir.equals(0))
                    {
                        _end.setY(_start.getY());
                        _end.setX(_start.getX() + b.getSize()-1);
                    }
                    else
                    {
                        _end.setX(_start.getX());
                        _end.setY(_start.getY() + b.getSize()-1);
                    }

                }while( !(cellIsFree(_start, b.getSize(), _dir) &&
                         _start.inGrid() &&
                         _end.inGrid()) );

                this.m_listBoat.get(this.m_listBoat.indexOf(b)).placeBoat(
                        new Cell(_start.getX(), _start.getY()),_dir);
            }
        }
        return true;
    }

    public void addAttack(Cell c)
    {
        if( c == null)
        {
            return;
        }

        if( this.justTouched == false ) // Choose randomly cell to attack between possible cell.
        {
            if( !this.m_maybeBoatHere.isEmpty())
            {
                int cellToAttack = (int)(Math.random() * this.m_maybeBoatHere.size()-1);
                c.setX(this.m_maybeBoatHere.get(cellToAttack).getX());
                c.setY(this.m_maybeBoatHere.get(cellToAttack).getY());

                Cell newCell = new Cell(c.getX(), c.getY());
                this.m_listCellAlreadyAttacked.add(newCell);
                this.m_maybeBoatHere.remove(c);
                this.m_possibleAttack.remove(c);

                updatePossibleAttack(newCell);
            }
            else
            {
                attackCross(c);
            }
        }
        else // On attaque les cases à coté de la dernière case attaquée.
        {
            this.m_maybeBoatHere.clear();
            attackLine(c);
        }

    }

    private void attackLine(Cell c)
    {
        Cell _lastAttack = this.m_listCellAlreadyAttacked.get(this.m_listCellAlreadyAttacked.size()-1);
        updatePossibleBoat(_lastAttack);
        if( this.m_maybeBoatHere.isEmpty() )
        {
            attackCross(c);
            return;
        }
        int cellToAttack = (int)(Math.random() * this.m_maybeBoatHere.size()-1);
        c.setX(this.m_maybeBoatHere.get(cellToAttack).getX());
        c.setY(this.m_maybeBoatHere.get(cellToAttack).getY());
        Cell newCell = new Cell(c.getX(), c.getY());
        this.m_listCellAlreadyAttacked.add(newCell);
        this.m_possibleAttack.remove(c);
        this.m_maybeBoatHere.remove(c);

        updatePossibleAttack(newCell);
    }

    private void attackCross(Cell c)
    {
        if(this.m_possibleAttack.isEmpty())
        {
            return;
        }
        int cellToAttack = (int)(Math.random() * this.m_possibleAttack.size()-1);

        c.setX(this.m_possibleAttack.get(cellToAttack).getX());
        c.setY(this.m_possibleAttack.get(cellToAttack).getY());
        Cell newCell = new Cell(c.getX(), c.getY());
        this.m_listCellAlreadyAttacked.add(newCell);
        this.m_possibleAttack.remove(cellToAttack);

        updatePossibleAttack(newCell);
    }

    private Cell firstAttack()
    {
        return new Cell((int)(Math.random() * 8), (int)(Math.random() * 8));
    }

    private void updatePossibleBoat(Cell _cell)
    {
        // Case du dessus.
        Cell top = new Cell(_cell.getX(), _cell.getY()-1);
        if( top.inGrid() && !this.m_listCellAlreadyAttacked.contains(top))
        {
            this.m_maybeBoatHere.add(top);
        }

        // Case du dessous.
        Cell bottom = new Cell(_cell.getX(), _cell.getY()+1);
        if( bottom.inGrid() && !this.m_listCellAlreadyAttacked.contains(bottom))
        {
            this.m_maybeBoatHere.add(bottom);
        }

        // Case de gauche.
        Cell left = new Cell(_cell.getX()-1, _cell.getY());
        if( left.inGrid() && !this.m_listCellAlreadyAttacked.contains(left))
        {
            this.m_maybeBoatHere.add(left);
        }

        // Case de droite.
        Cell right = new Cell(_cell.getX()+1,_cell.getY());
        if( right.inGrid() && !this.m_listCellAlreadyAttacked.contains(right))
        {
            this.m_maybeBoatHere.add(right);
        }
    }

    private void updatePossibleAttack(Cell c)
    {
        // Case du dessus à gauche.
        Cell topLeft = new Cell(c.getX()-1, c.getY()-1);
        if( topLeft.inGrid() &&
            !this.m_listCellAlreadyAttacked.contains(topLeft) &&
            !this.m_possibleAttack.contains(topLeft))
        {
            this.m_possibleAttack.add(topLeft);
        }

        // Case du dessus à droite.
        Cell topRight = new Cell(c.getX()+1, c.getY()-1);
        if( topRight.inGrid() &&
            !this.m_listCellAlreadyAttacked.contains(topRight) &&
            !this.m_possibleAttack.contains(topRight))
        {
            this.m_possibleAttack.add(topRight);
        }

        // Case en dessous à gauche.
        Cell bottomLeft = new Cell(c.getX()-1, c.getY()+1);
        if( bottomLeft.inGrid() &&
            !this.m_listCellAlreadyAttacked.contains(bottomLeft)&&
            !this.m_possibleAttack.contains(bottomLeft))
        {
            this.m_possibleAttack.add(bottomLeft);
        }

        // Case en dessous à droite.
        Cell bottomRight = new Cell(c.getX()+1, c.getY()+1);
        if( bottomRight.inGrid() &&
            !this.m_listCellAlreadyAttacked.contains(bottomRight) &&
            !this.m_possibleAttack.contains(bottomRight))
        {
            this.m_possibleAttack.add(bottomRight);
        }

    }
}
