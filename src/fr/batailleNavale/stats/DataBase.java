package fr.batailleNavale.stats;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBase extends SQLiteOpenHelper {

	// Table TRecapPlayer and its field
	private static final String TAB_PLAYER = "Tplayer";
	private static final String COL_ID = "PLAYERID";
	private static final String COL_NAME = "NAME";
	private static final String COL_WIN = "WIN";
	private static final String COL_SUMGAME = "SUMGAME";
	private static final String COL_MOVE = "MOVE";

	// Complete request for create TPlay
	private static final String CREATE_TABLE_PLAYER = "CREATE TABLE "
			+ TAB_PLAYER + " (" + COL_ID
			+ " INTEGER PRIMARY KEY AUTOINCREMENT, " + COL_NAME
			+ " TEXT NOT NULL, " + COL_WIN + " INTEGER," + COL_SUMGAME
			+ " INTEGER," + COL_MOVE + " INTEGER);";

	public DataBase(Context context, String name, CursorFactory factory,
			int version) {
		super(context, name, factory, version);
	}

	/**
	 * Cette méthode est appelée lors de la toute première création de la base
	 * de données. Ici, on doit créer les tables et éventuellement les populer.
	 */
	@Override
	public void onCreate(SQLiteDatabase db) {
		// on crée la table Tplayer dans la BDD
		db.execSQL(CREATE_TABLE_PLAYER);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// on supprime la table table_contacts de la BDD et on recrée la BDD
		db.execSQL("DROP TABLE " + TAB_PLAYER + ";");
		onCreate(db);
	}
	public void delete(SQLiteDatabase db) {
		// on supprime la table table_contacts de la BDD et on recrée la BDD
		db.execSQL("DROP TABLE " + TAB_PLAYER + ";");
		onCreate(db);
	}

}