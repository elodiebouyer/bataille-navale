package fr.batailleNavale.stats;

import android.util.Log;

@SuppressWarnings("rawtypes")
public class RecapPlayer implements Comparable{
	
	private static final String TAG = "RecapPlayer";
	
	private int id;
	private String name;
	private int sumGame;
	private int win;
	private int move;
	
	RecapPlayer(){}
	
	public RecapPlayer(String name, int win, int sumGame, int move){
		this.id = -1;
		this.name = name;
		this.win = win;
		this.sumGame = sumGame;
		this.move = move;
	}
	

	public RecapPlayer(String name) {
		this.id = -1;
		this.name = name;
		this.win = -1;
		this.move = -1;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getWin() {
		return win;
	}

	public void setWin(int win) {
		this.win = win;
	}

	public int getMove() {
		return move;
	}

	public void setMove(int move) {
		this.move = move;
	}

	public int getSumGame() {
		return sumGame;
	}

	public void setSumGame(int sumGame) {
		this.sumGame = sumGame;
	}

	@Override
	public int compareTo(Object other) {
		 int nombre1 = ((RecapPlayer) other).getWin(); 
	      int nombre2 = this.getWin(); 
	      Log.d(TAG, nombre1 + " compareTo " + nombre2);
	      if (nombre1 > nombre2)  return -1; 
	      else if(nombre1 == nombre2) return 0; 
	      else return 1; 
	}
	
	
	
	
}
