package fr.batailleNavale.stats;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class RecapPlayerBDD {
	private static final String TAG = "RecapPlayerBDD";

	private static final int VERSION_BDD = 1;
	private static final String NAME_BDD = "static.db";

	private static final String TAB_PLAYER = "Tplayer";

	private static final String COL_ID = "PLAYERID";
	private static final int NUM_COL_ID = 0;

	private static final String COL_NAME = "NAME";
	private static final int NUM_COL_NAME = 1;

	private static final String COL_WIN = "WIN";
	private static final int NUM_COL_WIN = 2;

	private static final String COL_SUMGAME = "SUMGAME";
	private static final int NUM_COL_SUMGAME = 3;

	private static final String COL_MOVE = "MOVE";
	private static final int NUM_COL_MOVE = 4;

	private String[] allColumns = { COL_ID, COL_NAME, COL_WIN, COL_SUMGAME,
			COL_MOVE };
	private SQLiteDatabase bdd;

	private DataBase myBaseSQLite;

	public RecapPlayerBDD(Context context) {
		// BDD Creation
		this.myBaseSQLite = new DataBase(context, NAME_BDD, null, VERSION_BDD);
	}

	public void open() {
		// Opening BDD write
		bdd = myBaseSQLite.getWritableDatabase();
	}

	public void close() {
		// Closing access to BDD
		bdd.close();
	}

	public SQLiteDatabase getBDD() {
		return bdd;
	}
	
	public DataBase getDataBase(){
		return this.myBaseSQLite;
	}

	/**
	 * Insert RecapPlayer on data base
	 * 
	 * @param player
	 *            insert player
	 * @return player's id
	 */
	public long insertRecapPlayer(RecapPlayer player) {

		ContentValues values = new ContentValues();

		values.put(COL_NAME, player.getName());
		values.put(COL_WIN, player.getWin());
		values.put(COL_SUMGAME, player.getSumGame());
		values.put(COL_MOVE, player.getMove());
		return bdd.insert(TAB_PLAYER, null, values);
	}

	/**
	 * Update RecapPlayer on data base
	 * 
	 * @param id
	 *            id's player to change
	 * @param player
	 *            player with change
	 * @return number of modifies lines
	 */
	public int updateRecapPlayer(int id, RecapPlayer player) {
		ContentValues values = new ContentValues();
		values.put(COL_NAME, player.getName());
		values.put(COL_WIN, player.getWin());
		values.put(COL_SUMGAME, player.getSumGame());
		values.put(COL_MOVE, player.getMove());
		return bdd.update(TAB_PLAYER, values, COL_ID + " = " + id, null);
	}

	/**
	 * Delete player with id on data base
	 * 
	 * @param id
	 *            id's player to delete
	 * @return number of player delete
	 */
	public int removeContactWithID(int id) {
		return bdd.delete(TAB_PLAYER, COL_ID + " = " + id, null);
	}

	/**
	 * Return the RecapPlayer by search with name.
	 * 
	 * @param name
	 *            player's name
	 * @return RecapPlayer on data base
	 */
	public RecapPlayer getPlayerWithName(String name) {
		Log.d(TAG, "getPlayerWithName(String name)");
		Cursor c = bdd.query(TAB_PLAYER, new String[] { COL_ID, COL_NAME,
				COL_WIN, COL_SUMGAME, COL_MOVE }, COL_NAME + " LIKE \"" + name
				+ "\"", null, null, null, null, null);
		return cursorToPlayerName(c, name);
	}

	private RecapPlayer cursorToPlayerName(Cursor c, String name) {
		// if no result, return null
		if (c.getCount() == 0)
			return null;

		// if result, go on the first element
		c.moveToFirst();
		RecapPlayer player = new RecapPlayer();
		player.setId(c.getInt(NUM_COL_ID));
		player.setName(c.getString(NUM_COL_NAME));
		player.setWin(c.getInt(NUM_COL_WIN));
		player.setSumGame(c.getInt(NUM_COL_SUMGAME));
		player.setMove(c.getInt(NUM_COL_MOVE));

		return player;
	}

	public List<RecapPlayer> getAllRecapPlayer() {

		List<RecapPlayer> allScore = new ArrayList<RecapPlayer>();

		Cursor cursor = bdd.query(TAB_PLAYER, allColumns, null, null, null,
				null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			RecapPlayer player = cursorToPlayer(cursor);
			allScore.add(player);
			cursor.moveToNext();
		}
		// make sure to close the cursor
		cursor.close();
		return allScore;
	}

	private RecapPlayer cursorToPlayer(Cursor cursor) {
		RecapPlayer player = new RecapPlayer();
		player.setId(cursor.getInt(NUM_COL_ID));
		player.setName(cursor.getString(NUM_COL_NAME));
		player.setWin(cursor.getInt(NUM_COL_WIN));
		player.setSumGame(cursor.getInt(NUM_COL_SUMGAME));
		player.setMove(cursor.getInt(NUM_COL_MOVE));

		return player;
	}
}
