package fr.batailleNavale.stats;

import java.util.Collections;
import java.util.List;

import fr.batailleNavale.game.players.IPlayer;

// Class to exchange with dataBase
@SuppressWarnings("unused")
public class ExchangeBDD {

	/**
	 * This function is used to update on entry of player on database. If the
	 * entry not exist, it is create.
	 * 
	 * @param _p
	 *            Player to update or create.
	 * 
	 * @param _playerBdd
	 *            BDD to make change [RecapPlayerBDD playerBdd = new
	 *            RecapPlayerBdd(this)] <-- On Activity calling this function;
	 * 
	 **/
	public void UpdatePLayerScore(IPlayer _p, RecapPlayerBDD _playerBdd) {

		String name = _p.getName();
		int move = _p.getAllCellsAttacked().size();
		//int move = 0;
		int win = 0;
		int sumGame = 1;

		_playerBdd.open();

		// search all data about RecapPlayer p
		RecapPlayer rp = _playerBdd.getPlayerWithName(name);

		// if player rp exist on data base, update his content
		if (rp != null) {

			if (!_p.win()) {
				rp.setWin(rp.getWin() + 1);
			}
			rp.setSumGame(rp.getSumGame() + 1);
			
			if(rp.getMove() > move){
				rp.setMove(move); 
			}
			else{
				rp.setMove(rp.getMove());
			}
			_playerBdd.updateRecapPlayer(rp.getId(), rp);
		}
		// if player rp not exist on data base, create him.
		else {
			if (!_p.win()) {
				win = 1;
			}
			sumGame = 1;

			rp = new RecapPlayer(name, win, sumGame, move);

			_playerBdd.insertRecapPlayer(rp);

		}
		_playerBdd.close();
	}

	/**
	 * Obtain all score
	 * 
	 * @param _playerBdd
	 *            BDD to make change [RecapPlayerBDD playerBdd = new
	 *            RecapPlayerBdd(this)] <-- On Activity calling this function;
	 * 
	 * @return allScore List of all player on database
	 * 
	 **/
	public List<RecapPlayer> getAllBasicScore(RecapPlayerBDD _playerBdd) {
		_playerBdd.open();
		List<RecapPlayer> allScore = _playerBdd.getAllRecapPlayer();
		_playerBdd.close();
		Collections.sort(allScore, Collections.reverseOrder());
		return allScore;

	}

}
