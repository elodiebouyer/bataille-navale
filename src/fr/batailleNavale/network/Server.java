package fr.batailleNavale.network;

import android.os.Handler;
import android.util.Log;

import org.apache.http.conn.util.InetAddressUtils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.Enumeration;
import java.util.EventObject;

import fr.batailleNavale.network.Event.EventClient;
import fr.batailleNavale.network.Event.EventClientListener;
import fr.batailleNavale.network.Event.EventIP;
import fr.batailleNavale.network.Event.EventIPListener;
import fr.batailleNavale.network.Event.EventRecvMesg;
import fr.batailleNavale.network.Event.EventRecvMesgListener;
import fr.batailleNavale.network.Event.EventSendMesg;
import fr.batailleNavale.network.Event.EventSendMesgListener;

/**
 * Created by elodie on 27/12/13.
 */
public class Server implements EventSendMesgListener
{
    private ServerSocket serverSocket;
    private Socket clientSocket = null;
    private Handler handler = null;
    private Thread thread = null;
    private String serverName;
    private int SERVERPORT;
    private String msgRecv;

    EventIPListener ipListener;
    EventClientListener clientListener;
    EventRecvMesgListener recvMesgListener;
    EventSendMesgListener sendMesgListener;
    private BufferedReader input;
    private PrintWriter printwriter;

    public synchronized void addEventIPListener(EventIPListener listener)
    {
        this.ipListener = listener;
    }

    public synchronized void addEventClientListener(EventClientListener listener)
    {
        this.clientListener = listener;
    }

    public synchronized void addEventRecvMesgListener(EventRecvMesgListener listener)
    {
        this.recvMesgListener = listener;
    }

    public synchronized void addEventSendMesgListener(EventSendMesgListener listener)
    {
        this.sendMesgListener = listener;
    }

    /**
     * Call when the ip address is available.
     */
    private synchronized void fireEventIP()
    {
        this.ipListener.handleEventIp(new EventIP(this));
    }

    private synchronized void fireEventRecvMesg()
    {
        this.recvMesgListener.handleEventRecvMesg(new EventRecvMesg(this));
    }

    private synchronized void fireEventSendMesg(String typeMsg)
    {
        this.sendMesgListener.handleEventSendMesg(new EventSendMesg(this, typeMsg));
    }

    /**
     * Call when the client is connected.
     */
    private synchronized void fireEventClient()
    {
        this.clientListener.handleEventClient(new EventClient(this));
    }

    public Server(String name, int port)
    {
        this.serverName = name;
        SERVERPORT = port;
        addEventSendMesgListener(this);
        if( this.handler == null ) this.handler = new Handler();
        if( this.thread == null )
        {
            this.thread = new Thread(new ServerThread());
            this.thread.start();
        }
    }

    public String getServerName()
    {
        return serverName;
    }

    public String getMsgRecv()
    {
        return this.msgRecv;
    }

    private String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress() && InetAddressUtils.isIPv4Address(inetAddress.getHostAddress()) ) {
                    	return inetAddress.getHostAddress();
                    }
                }
            }
        } catch (SocketException ex) {
            Log.e("Serveur", ex.toString());
        }
        return null;
    }

    public String getipAddress()
    {
        if(serverSocket != null && serverSocket.isBound()) return getLocalIpAddress();
        return "Adresse non disponible.";
    }

    @Override
    public void handleEventSendMesg(EventObject e) {

    }

    public void stop()
    {
        try {
            serverSocket.close();
            clientSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    class ServerThread implements Runnable {
 
        public void run()
        {
            try
            {
                serverSocket = new ServerSocket(SERVERPORT);
                fireEventIP(); // Adresse dispo.

                // Tant que le serveur n'est pas interrompu.
                while (!Thread.currentThread().isInterrupted())
                {
                    try
                    {
                        clientSocket = serverSocket.accept(); // On accepte la connection avec le client.
                        input = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                        CommunicationThread commThread = new CommunicationThread();
                        new Thread(commThread).start();
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                        Log.e("------Server------", "Problème avec la socket client.");
                    }
                }
            }
            catch (IOException e)
            {
                Log.e("Serveur", "Problème avec le port.");
                e.printStackTrace();
            }

        }
    }

    public void sendMsg(String msg)
    {
        try
        {
            printwriter = new PrintWriter(new BufferedWriter(
                    new OutputStreamWriter(clientSocket.getOutputStream())), true);
            printwriter.println(msg);
            printwriter.flush();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Traitement des messages envoyés et reçus.
     */
    class CommunicationThread implements Runnable
    {
        public void run()
        {
            while (!Thread.currentThread().isInterrupted())
            {
                try
                {
                    Log.i("----------------------Server-------------------", "Attente message");
                    String read = input.readLine();
                    handler.post(new updateUIThread(read));
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
        }
    }

    class updateUIThread implements Runnable
    {
        String msg;
        public updateUIThread(String str)
        {
            this.msg = str;
        }

        @Override
        public void run()
        {
            msgRecv = msg;
            if( msg.equals(Protocole.P_CONNECT)) fireEventClient();
            else fireEventRecvMesg();
        }
    }

}
