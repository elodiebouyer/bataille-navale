package fr.batailleNavale.network;

/**
 * Created by elodie on 08/01/14.
 */
public class Protocole
{
    public static final String P_PSEUDO = "P_PSEUDO";
    public static final String P_CONNECT = "P_CONNECT";
    public static final String P_BOAT = "P_BOAT";
    public static final String P_CELL = "P_CELL";
    public static final String P_TOUCHED = "P_TOUCHED";
    public static final String P_LOSE = "P_LOSE";
};
