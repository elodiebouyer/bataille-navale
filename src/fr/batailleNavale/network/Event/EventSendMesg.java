package fr.batailleNavale.network.Event;

import java.util.EventObject;

/**
 * Created by elodie on 08/01/14.
 */
public class EventSendMesg extends EventObject
{
    public String typeMsg;
    /**
     * Constructs a new instance of this class.
     *
     * @param source the object which fired the event.
     */
    public EventSendMesg(Object source, String type)
    {
        super(source);
        typeMsg = type;
    }
}
