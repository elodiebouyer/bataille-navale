package fr.batailleNavale.network.Event;

import java.util.EventObject;

/**
 * Created by elodie on 08/01/14.
 */
public interface EventRecvMesgListener
{
    public void handleEventRecvMesg(EventObject e);
}
