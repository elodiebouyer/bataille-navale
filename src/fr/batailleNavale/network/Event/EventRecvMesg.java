package fr.batailleNavale.network.Event;

import java.util.EventObject;

/**
 * Created by elodie on 08/01/14.
 */
public class EventRecvMesg extends EventObject
{
    /**
     * Constructs a new instance of this class.
     *
     * @param source the object which fired the event.
     */
    public EventRecvMesg(Object source) {
        super(source);
    }
}
