package fr.batailleNavale.network.Event;

import java.util.EventObject;

/**
 * Created by elodie on 07/01/14.
 */
public interface EventBadConnectionListener
{
    public void handleEventBadConnection(EventObject e);
}
