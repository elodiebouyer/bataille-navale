package fr.batailleNavale.network.Event;

import java.util.EventObject;

/**
 * Created by elodie on 07/01/14.
 */
public interface EventClientListener
{
    public void handleEventClient(EventObject e);
}
