package fr.batailleNavale.network.Event;

import java.util.EventObject;

/**
 * Created by elodie on 07/01/14.
 */
public interface EventIPListener
{
    public void handleEventIp(EventObject e);
}
