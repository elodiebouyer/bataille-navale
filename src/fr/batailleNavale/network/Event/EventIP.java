package fr.batailleNavale.network.Event;

import java.util.EventObject;

/**
 * Created by elodie on 07/01/14.
 */
public class EventIP extends EventObject
{
    /**
     * Constructs a new instance of this class.
     *
     * @param source the object which fired the event.
     */
    public EventIP(Object source) {
        super(source);
    }
}
