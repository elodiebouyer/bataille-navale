package fr.batailleNavale.network;

import android.os.Handler;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
    import java.net.Socket;
import java.util.EventObject;

import fr.batailleNavale.network.Event.EventBadConnection;
import fr.batailleNavale.network.Event.EventBadConnectionListener;
import fr.batailleNavale.network.Event.EventGoodConnection;
import fr.batailleNavale.network.Event.EventGoodConnectionListener;
import fr.batailleNavale.network.Event.EventRecvMesg;
import fr.batailleNavale.network.Event.EventRecvMesgListener;
import fr.batailleNavale.network.Event.EventSendMesg;
import fr.batailleNavale.network.Event.EventSendMesgListener;

/**
 * Created by elodie on 27/12/13.
 */
public class Client implements EventSendMesgListener
{
    private Socket socketServeur;
    private Handler handler = null;
    private int SERVERPORT;
    private String server_IP;
    private String clientName;
    private String msgRecv;
    private Thread threadClient;
    private BufferedReader input;
    private PrintWriter printwriter;

    private EventBadConnectionListener badConnectionListener;
    private EventGoodConnectionListener goodConnectionListener;
    private EventRecvMesgListener recvMesgListener;
    EventSendMesgListener sendMesgListener;

    public synchronized void addEventBadConnectionListener(EventBadConnectionListener listener)
    {
        this.badConnectionListener = listener;
    }

    public synchronized void addEventGoodConnectionListener(EventGoodConnectionListener listener)
    {
        this.goodConnectionListener = listener;
    }

    public synchronized void addEventRecvMesgListener(EventRecvMesgListener listener)
    {
        this.recvMesgListener = listener;
    }

    public synchronized void addEventSendMesgListener(EventSendMesgListener listener)
    {
        this.sendMesgListener = listener;
    }

    private synchronized void fireEventBadConnection()
    {
        this.badConnectionListener.handleEventBadConnection(new EventBadConnection(this));
    }

    private synchronized void fireEventGoodConnection()
    {
        this.goodConnectionListener.handleEventGoodConnection(new EventGoodConnection(this));
    }

    private synchronized void fireEventRecvMesg()
    {
        this.recvMesgListener.handleEventRecvMesg(new EventRecvMesg(this));
    }

    private synchronized void fireEventSendMesg(String type)
    {
        this.sendMesgListener.handleEventSendMesg(new EventSendMesg(this, type));
    }

    public Client(String ip, int port, String n)
    {
        this.clientName = n;
        if( this.handler == null ) this.handler = new Handler();
        this.server_IP = ip;
        this.SERVERPORT = port;
        this.threadClient = new Thread(new ClientThread());
        this.threadClient.start();
    }

    public String getClientName()
    {
        return clientName;
    }

    public String getMsgRecv()
    {
        return this.msgRecv;
    }

    public void sendMsg(String msg)
    {
        try
        {
	            printwriter = new PrintWriter(new BufferedWriter(
	                    new OutputStreamWriter(socketServeur.getOutputStream())), true);
	            printwriter.println(msg);
	            printwriter.flush();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    /**
     * Call when the server send msg.
     * @param e
     */
    @Override
    public void handleEventSendMesg(EventObject e)
    {
        CommunicationThread commThread = new CommunicationThread();
        new Thread(commThread).start();
    }

    class ClientThread implements Runnable
    {
        @Override
        public void run()
        {
            try
            {
                socketServeur = new Socket(server_IP, SERVERPORT);
                input = new BufferedReader(new InputStreamReader(socketServeur.getInputStream()));
                CommunicationThread commThread = new CommunicationThread();
                new Thread(commThread).start();
                fireEventGoodConnection();
                sendMsg(Protocole.P_CONNECT);
            }
            catch (Exception e1)
            {
                e1.printStackTrace();
                fireEventBadConnection();
            }
        }
    }

    class CommunicationThread implements Runnable
    {
        public void run()
        {
            while (!Thread.currentThread().isInterrupted())
            {
                try
                {
                    String read = input.readLine();
                    handler.post(new updateUIThread(read));
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        }
    }

    class updateUIThread implements Runnable
    {
        String msg;
        public updateUIThread(String str)
        {
            this.msg = str;
        }

        @Override
        public void run()
        {
            msgRecv = msg;
            fireEventRecvMesg();
        }
    }


}
