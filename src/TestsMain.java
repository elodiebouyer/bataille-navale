import fr.tests.BoatTest;
import fr.tests.CellTest;
import fr.tests.PlayerTest;

class TestsMain
{

    private static void testBoat()
    {
        System.out.println("\n Boat tests :: ");
        BoatTest.init();
        BoatTest.sizeTest();
        BoatTest.noSankTest();
        BoatTest.placeBoatTest();
        BoatTest.attackBoat();
        BoatTest.sankTest();
    }

    private static void testCell()
    {
        System.out.println("\n Cell tests :: ");
        CellTest.init();
        CellTest.coordinateTest();
        CellTest.inGridTest();
        CellTest.equalTest();
        CellTest.haschCodeTest();
    }

    public static void testPlayer()
    {
        System.out.println("\n Player tests :: ");
        PlayerTest.init();
        PlayerTest.nbBoatTest();
        PlayerTest.placeBoatHumanTest();
        PlayerTest.placeBoatIATest();
        PlayerTest.attackHumanTest();
        PlayerTest.attackIATest();
    }

    public static void main (String[] args)
    {
        testBoat();
        testCell();
        testPlayer();
    }
}